(include "meta.rkt")

(define empty-list '())
(define char_s " ")

(define (print-rec r-kw-list lvl)
  (for-each (lambda (v) (if (list? v)
                            (begin (printf (string-append (str-mul char_s (* (+ lvl 1) 3)) "}"))
                                   (printf "\n")
                                   (print-rec v (+ lvl 1)))
                            (if (pair? v)
                                (printf (string-append "~a" (str-mul char_s (* lvl 3)) "~a : ~a\n")
                                        (to-string lvl)
                                        (to-string (kw v))
                                        (to-string (val v)))
                                (printf "ERROR"))))
            r-kw-list)
  (printf (string-append (str-mul char_s (* lvl 3)) "}\n")))

(define (enter-kw-list kw-list)
  ;; IT CANNOT BE NULL!
  (if (null? kw-list) kw-list

      ;; SHORTCUTSA
      (let ((current-group (get-kw-group kw-list))
            (remaining-groups (next kw-list)))
        
        (cond ;; POSSIBLE VALUES: (, ), keyword, number
          
          ;; END LIST
          ;; Returns the remaining list values and
          ;; ends recursion
          ((equal? (kw current-group) 'close-paran)
           (list (lambda () remaining-groups)))

          ;; START NEW LIST
          ;; A open param defines a new list
          ;; hence, values should be added recursivly
          ((equal? (kw current-group) 'open-paran)
           (let ((next-list-val (enter-kw-list remaining-groups)))
             ;; GOING BACK FROM RECURSION LEVELS
             ;; A lambda expression containing the remaining
             ;; list values to parse
             (if (procedure? (list-ref next-list-val 
                                       (- (length next-list-val) 1)))
                 ;; EXPECTING MORE ARGUMENTS
                 ;; Continue on creating the list of values
                 (cons (list-until (- (length next-list-val) 1) next-list-val)
                       (enter-kw-list ((list-ref next-list-val

                                                 (- (length next-list-val) 1)))))
                 ;; LIST HAS NO MORE VALUES
                 ;; Complete the list
                 (cons list-until '()))))

          ;; NUMBER VALUE
          ;; Just evaluate to itself and continue the list
          ((equal? (kw current-group) 'number)
           (cons current-group (enter-kw-list remaining-groups)))

          ;; KEYWORD VALUE
          ;; Just evaluate to itself and continue the list          
          ((equal? (kw current-group) 'keyword)
           (cons current-group (enter-kw-list remaining-groups)))))))

(define (structurize kw-list)
  ;; IT CANNOT BE NULL!
  (if (null? kw-list) kw-list

      ;; SHORTCUTSA
      (let ((current-group (get-kw-group kw-list))
            (remaining-groups (next kw-list)))
        
        (cond ;; POSSIBLE VALUES: (, ), keyword, number
          
          ;; END LIST
          ;; Returns the remaining list values and
          ;; ends recursion
          ((equal? current-group 'close-paran)
           (list (lambda () remaining-groups)))
          
          ;; START NEW LIST
          ;; A open param defines a new list
          ;; hence, values should be added recursivly
          ((equal? current-group 'open-paran)
           (let ((next-list-val (structurize remaining-groups)))
             ;; GOING BACK FROM RECURSION LEVELS
             ;; A lambda expression containing the remaining
             ;; list values to parse
             (if (procedure? (list-ref next-list-val 
                                       (- (length next-list-val) 1)))
                 ;; EXPECTING MORE ARGUMENTS
                 ;; Continue on creating the list of values
                 (cons (list-until (- (length next-list-val) 1) next-list-val)
                       (structurize ((list-ref next-list-val
                                               (- (length next-list-val) 1)))))
                 ;; LIST HAS NO MORE VALUES
                 ;; Complete the list
                 (cons list-until '()))))

          ;; Constant number
          ;; Just evaluate to itself and continue the list
          ((const_num? current-group)
           (cons current-group
                 (structurize remaining-groups)))

          ;; Variable or predefined function / constant.
          ;; Just evaluate to itself and continue the list          
          ((atom? current-group)
           (cons current-group
                 (structurize remaining-groups)))))))


(define (sep-kw-list str)
  ;; Return a list of stings containing
  ;; Either a (left / right) paran
  ;; Or a keyword
  (filter (lambda (x) (not (string=? "" x)))
          ;; Separate keywords and parans
          (regexp-split (regexp " ")
                        (string-replace (string-replace str
                                                        "("
                                                        " ( ")
                                        ")"
                                        " ) "))))


(define (iter-kw-list kw-list)
  (map (lambda (kw-obj)
         ;; Caracterize all the contents of the kw-list and 
         ;; return a group containing the content and an
         ;; identified caracteration.
         (cond ((string=? kw-obj "(") 'open-paran)
               ((string=? kw-obj ")") 'close-paran)
               ;; If kw-obj is not a paran then its either a 
               ;; keyword or a number.
               (else (let ((num (string->number kw-obj)))
                       (if (number? num)
                           (kw-group 'number num)
                           (kw-group 'keyword kw-obj))))))
       kw-list))

(define (iter-kw-list2 kw-list)
  (map (lambda (kw-obj)
         ;; Caracterize all the contents of the kw-list and 
         ;; return a group containing the content and an
         ;; identified caracteration.
         (cond ((string=? kw-obj "(") 'open-paran)
               ((string=? kw-obj ")") 'close-paran)
               ;; If kw-obj is not a paran then its either a 
               ;; keyword or a number.
               (else (let ((num (string->number kw-obj)))
                       (if (number? num)
                           (const_num num)
                           (atom kw-obj))))))
       kw-list))



(define (constant? x)
  ;; Is the given value x a constant (number or variable)? 
  (if (not (pair? x)) #f
      (let ((atom (kw x)))
        (if (or (equal? atom 'keyword) (equal? atom 'number))
            #t
            #f))))

(define (parse str)
  ;; STRING -> sexpr of kw
  ;; Convert a string into list of lists 
  (enter-kw-list (iter-kw-list (sep-kw-list str))))

(define (deep-parse str)
  (structurize (iter-kw-list2 (sep-kw-list str))))

