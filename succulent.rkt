#lang racket
;; One may edit this software under the Gplv2 (or later) version

(require racket/include)
(include "evaluation.rkt")

(define (how-many items char)
  (define r (regexp (format "[^~a]" char)))
  (for/sum ((text (in-list items)))
    (string-length (regexp-replace* r text ""))))






(define (READ prompt)
  (define (iter input)
    (printf prompt)
    (let ((typed (read-line)))
      (if (= (how-many (list typed input) #\()
             (how-many (list typed input) #\)))
          (string-append input typed)
          (iter (string-append input typed)))))
  (iter ""))
 
 (define (EVAL expr) ;; Evaluate the input
  (evaluate (create_statement (car (deep-parse expr))
                              global_state)
            global_state))
                           
(define (PRINT return_value) ;; Prints the sorted structure
  (printf "\n")
  (cond ((const_num? return_value)
         (printf (number->string (const_num-value return_value))))
        ((atom? return_value)
         (printf (atom-name return_value)))
        ((func_lambda? return_value)
         (printf "LAMBDA_FUNCTION")))
  (printf "\n"))



(define (REPL-main state) 
  (begin (let ((input (READ "")))
           (if (not (string=? input "")) 
               (begin (PRINT (EVAL input))
                      (printf "λ>")
                      (REPL-main state))  
               (begin (printf "λ>")
                      (REPL-main state))
               ))))
(define (REPL) 
  (printf (string-append "Welcome to Succulent v" version "\n"))
  (REPL-main 0))
